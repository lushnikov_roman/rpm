package id;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Id {
    public static void main(String[] args) throws IOException {
        String listLine = readFile();
        Pattern pattern = Pattern.compile("[a-zA-Z_][a-zA-Z_0-9]*");
        ArrayList<String> patterns = search(pattern, listLine);
        out((HashSet<String>)identificators(patterns));
        System.out.println("\n");
        out(patterns);
    }

    public static String readFile() throws IOException {
        String string="";
        List<String> strings = Files.readAllLines(Paths.get("src/book/Main.java"));
        for (int i = 0; i < strings.size(); i++) {
            string = string.concat(strings.get(i));
        }
        return string;
    }

    private static ArrayList<String> search(Pattern pattern, String listLine) {
        ArrayList<String> patterns = new ArrayList<>();
        Matcher matcher = pattern.matcher(listLine);
        while (matcher.find()) {
            patterns.add(String.valueOf(Pattern.compile(matcher.group())));
        }
        return patterns;
    }

    private static Set<String> identificators(ArrayList<String> template) {
        HashSet<String> patterns = new HashSet<>();
        patterns.addAll(template);
        return patterns;
    }

    private static void out(HashSet<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }

    private static void out(ArrayList<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }
}

