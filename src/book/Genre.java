package book;

public enum Genre {
    FANTASY,
    HORRORS,
    DOCUMENTARY,
    FIGHTER,
    MELODRAMA
}
