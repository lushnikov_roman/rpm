package bank;

public class Account {
    private final String number;
    private final String owner;
    private int amount;

    public Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    public Account(final String number, final String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public int getAmount() {
        return amount;
    }

    private int withdraw(int amountToWithdraw) {
        int error = 0;
        if (amountToWithdraw < 0) {
            error = 1;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return 2;
        }
        amount = amount - amountToWithdraw;
        return error;
    }

    private int put(int amountToPut) {
        int error = 0;
        if (amountToPut < 0) {
            return 1;
        }
        amount = amount + amountToPut;
        return error;
    }

    /**
     * Внутренний класс
     * Inner class
     */
    public class Card {
        private final String number;

        public Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        public int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        public int put(final int amountToPut) {
            return Account.this.put(amountToPut);
        }
    }
}
