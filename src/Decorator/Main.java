package Decorator;

import Decorator.decorators.NoTopping;
import Decorator.decorators.ToppingSprinkles;
import Decorator.decorators.ToppingSyrup;
import Decorator.product.Component;
import Decorator.product.Ice;
import Decorator.product.Smoothies;
import Decorator.product.Yogurt;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Component ice;
        Component yogurt;
        Component smoothies;
        Random random = new Random();

        boolean showBorder = random.nextBoolean();

        if (showBorder) {
            showBorder = random.nextBoolean();
            if (showBorder) {
                ice = new ToppingSprinkles(new Ice());
            } else {
                ice = new ToppingSyrup(new Ice());
            }
        } else {
            ice = new Ice();
        }

        showBorder = random.nextBoolean();
        if (showBorder) {
            showBorder = random.nextBoolean();
            if (showBorder) {
                smoothies = new ToppingSprinkles(new Smoothies());
            } else {
                smoothies = new ToppingSyrup(new Smoothies());
            }
        } else {
            smoothies = new Smoothies();
        }

        showBorder = random.nextBoolean();
        if (showBorder) {
            yogurt = new NoTopping(new Yogurt());
        } else {
            yogurt = new Yogurt();
        }

        ice.topping();
        smoothies.topping();
        yogurt.topping();
    }
}