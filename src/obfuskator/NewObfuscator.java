package obfuskator;

import java.io.IOException;
import java.util.Scanner;

public class NewObfuscator {
    public static void main(String[] args) throws IOException {
        Scanner read = new Scanner(System.in);
        System.out.println("Введите путь: ");
        String fullPath = read.nextLine();
        Obfuscate.obfuscate(fullPath);
    }
}
