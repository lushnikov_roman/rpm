package Decorator.decorators;

import Decorator.product.Component;

public class NoTopping extends Decorator {
    public NoTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" -  без топпинга");
    }
}
