package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPv4ValidityExample11 {
    public static void main(String[] args) {
        int counter = 0;
        String string = "0.0.0.0";
        Pattern pattern1 = Pattern.compile("(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))");
        Matcher matcher1 = pattern1.matcher(string);

        while (matcher1.find()) {
            counter++;
            System.out.println("Match found " + string.substring(matcher1.start(), matcher1.end()) + " Starting at index " + matcher1.start() + " and ending at index " + matcher1.end());

        }
        System.out.println("Matches found " + counter);
    }
}
