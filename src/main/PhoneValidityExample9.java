package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidityExample9 {
    public static void main(String[] args) {

        String string = "+38044228228";
        Pattern pattern1 = Pattern.compile("^((\\+?380)([0-9]{9}))$");
        Matcher matcher1 = pattern1.matcher(string);

        if(matcher1.matches()){
            System.out.println("Phone number is correct");
        }else{
            System.out.println("Phone number is incorrect");
        }
    }
}