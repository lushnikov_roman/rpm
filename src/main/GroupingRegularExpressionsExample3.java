package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupingRegularExpressionsExample3 {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("[a-z]+");
        Matcher matcher1 = pattern1.matcher("a b c d f 1 2 3 4");
        Matcher matcher2 = pattern1.matcher("A B C D F 1 2 3 4");

        if(!matcher1.find()){
            System.out.println("Coincidence ot found");
        }

        while (matcher2.find()){
            System.out.println(matcher2.group());
        }
    }
}
