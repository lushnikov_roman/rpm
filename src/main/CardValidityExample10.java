package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardValidityExample10 {
    public static void main(String[] args) {

        String cardNumber = "2234 5678 9012 3456";
        String date = "03/12";
        String CVV = "123";
        Pattern cardNumberPattern = Pattern.compile("([2-6]([0-9]{3}) ?)(([0-9]{4} ?){3})");
        Pattern datePatter = Pattern.compile("(0[1-9]|1[0-2])/([0-9]{2})");
        Pattern CVVPattern= Pattern.compile("[0-9]{3}");
        Matcher cardNumberMatcher = cardNumberPattern.matcher(cardNumber);
        Matcher dateMatcher = datePatter.matcher(date);
        Matcher CVVMatcher = CVVPattern.matcher(CVV);

        if (cardNumberMatcher.matches() && dateMatcher.matches() && CVVMatcher.matches()) {
            System.out.println("Card  is correct");
        } else {
            System.out.println("Card is incorrect");
        }
    }
}
