package book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

@SuppressWarnings("all")

/**
 * В классе Main созданы книги и вызваны методы которые сортируют книги по определенным параметрам.
 */

public class Main {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        Book book1 = new Book("Трава", "Чуковсий", 2020, "С:книга1", Genre.FANTASY);
        Book book2 = new Book("Доктор", "Пушкин", 2021, "С:книга2", Genre.HORRORS);
        Book book3 = new Book("Земля", "Орлов", 2000, "С:книга3", Genre.DOCUMENTARY);
        Book book4 = new Book("Аист", "Лягушкин", 1999, "С:книга4", Genre.FIGHTER);
        Book book5 = new Book("Гном", "Пушкин", 1892, "С:книга5", Genre.MELODRAMA);
        Book book6 = new Book("Инфляция", "Зарёв", 3123, "С:книга6", Genre.HORRORS);

        ArrayList<Book> authorsBooks = new ArrayList<>();
        ArrayList<Book> genreBooks = new ArrayList<>();
        ArrayList<Book> booksNames = new ArrayList<>();
        ArrayList<Book> books = new ArrayList<>(Arrays.asList(book1, book2, book3, book4, book5, book6));

        String messageForSort = "Книги завези,а то сортировать нечего :(";
        String messageForFiltrationAuthor = "Книг такого автора не нашлось :(";
        String messageForFiltrationGenre = "Книг этого жанра не нашлось :(";
        String messageForSearchBook = "Книг с таким названием не нашлось :(";

        isEmpty(authorSorting(books), messageForSort);
        isEmpty(bookSorting(books), messageForSort);
        isEmpty(YearSorting(books), messageForSort);

        System.out.println("Введите автора ,чьи книги хотите увидеть: ");
        String author = read.nextLine();
        isEmpty(filtrationAuthor(books, authorsBooks, author), messageForFiltrationAuthor);

        System.out.println("Введите жанр книг ,которые хотите увидеть: ");
        String genre = read.nextLine();
        isEmpty(filtrationGenre(books, genreBooks, genre), messageForFiltrationGenre);

        System.out.println("Введите название книги ,которую хотите увидеть: ");
        String bookName = read.nextLine();
        isEmpty(searchBook(books, booksNames, bookName), messageForSearchBook);
    }

    /**
     * Метод authorSorting сортирует книги по авторам.
     *
     * @param books - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     * @return - возвращаем ArrayList.
     */

    private static ArrayList authorSorting(ArrayList<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = 1 + i; j < books.size(); j++) {
                int compare = books.get(j).getAuthor().compareTo(books.get(i).getAuthor());
                if (compare < 0) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }

                if (compare == 0) {
                    if (books.get(j).getDate() < books.get(i).getDate()) {
                        temp = books.get(i);
                        books.set(i, books.get(j));
                        books.set(j, temp);
                    }
                }
            }
        }
        return books;
    }

    private static ArrayList filtrationAuthor(ArrayList<Book> books, ArrayList<Book> authorsBooks, String author) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getAuthor().equals(author)) {
                authorsBooks.add(books.get(i));
            }
        }
        return authorsBooks;
    }

    private static ArrayList filtrationGenre(ArrayList<Book> books, ArrayList<Book> genreBooks, String genre) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getGenre().toString().equals(genre)) {
                genreBooks.add(books.get(i));
            }
        }
        return genreBooks;
    }

    private static ArrayList searchBook(ArrayList<Book> books, ArrayList<Book> booksNames, String bookName) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getBook().equals(bookName)) {
                booksNames.add(books.get(i));
            }
        }
        return booksNames;
    }

    private static ArrayList bookSorting(ArrayList<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = 1 + i; j < books.size(); j++) {
                int compare = books.get(j).getBook().compareTo(books.get(i).getBook());
                if (compare < 0) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }

                if (compare == 0) {
                    if (books.get(j).getDate() < books.get(i).getDate()) {
                        temp = books.get(i);
                        books.set(i, books.get(j));
                        books.set(j, temp);
                    }
                }
            }
        }
        return books;
    }

    /**
     * Метод birthYerSorting сортирует книги по авторам.
     *
     * @param books - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     * @return - возвращаем ArrayList.
     */

    private static ArrayList YearSorting(ArrayList<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = i + 1; j < books.size(); j++) {
                if (books.get(j).getDate() > books.get(i).getDate()) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
            }
        }
        return books;
    }

    private static void isEmpty(ArrayList<Book> arrayList, String message) {
        if (arrayList.isEmpty()) {
            System.out.println(message + "\n");
        } else {
            print(arrayList);
        }
    }

    /**
     * Метод нормального вывода отсортированных данных.
     *
     * @param arrayList - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     */

    private static void print(ArrayList<Book> arrayList) {
        for (Book book : arrayList) {
            System.out.println(book);
        }
        System.out.println();
    }
}
