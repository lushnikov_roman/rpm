package obfuskator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Obfuscate {
    private String fullPath;

    public Obfuscate(String fullPath) {
        this.fullPath = fullPath;
    }

     static void obfuscate(String fullPath) throws IOException {
        String s = "";
        String nameFile = fileName(fullPath);
        String path = pathNewFile(fullPath);
        s = readFile(fullPath);
        s = newNameСlass(s, nameFile);
        s = comment(s);
        s = space(s);
        fileWrite(s, path, nameFile);
    }

    public static String readFile(String path) throws IOException {
        String s="";
        List<String> strings = Files.readAllLines(Paths.get(path));
        for (int i = 0; i < strings.size(); i++) {
            s = s.concat(strings.get(i));
        }
        return s;
    }

    public static String newNameСlass(String string, String nameFile) {
        return string.replaceAll(nameFile, "New" + nameFile);
    }

    public static String fileName(String path) {
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        ArrayList<String> fileNames = new ArrayList<>(Arrays.asList(strings.get(strings.size() - 1).split("\\.")));
        String nameFile = fileNames.get(0);
        return nameFile;
    }

    private static String space(String stringFile) {
        return stringFile.replaceAll("\\s+(?![^\\d\\s])", "");
    }

    private static String comment(String stringFile) {
        return stringFile.replaceAll( "(/\\*.+?\\*/)|(//.+?)[:;a-zA-Zа-яА-ЯЁё]*","");
    }

    public static String pathNewFile(String path) {
        String pathNewFile = "";
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(path.split("/")));
        for (int i = 0; i < strings.size() - 1; i++) {
            pathNewFile = pathNewFile + strings.get(i) + "/";
        }
        return pathNewFile;
    }

    private static void fileWrite(String stringFile, String path, String nameFile) throws IOException {
        try (FileWriter writer = new FileWriter(path + "New" + nameFile + ".java")) {
            writer.write(stringFile);
        }
    }
}
