package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CharacterClassExample8 {
    public static void main(String[] args) {
        int counter=0;
        String string="abcd1234";
        Pattern pattern1 = Pattern.compile("\\p{Digit}");
        Matcher matcher1 = pattern1.matcher(string);

        while (matcher1.find()){
            counter++;
            System.out.println("Match found "+string.substring(matcher1.start(),matcher1.end())+" Starting at index "+matcher1.start() + "and ending at index "+ matcher1.end());

        }
        System.out.println("Matches found " +counter);
    }
}
