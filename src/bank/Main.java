package bank;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Account accountVTB = new Account("1234234534564567", "Tatiana Iniushkina", 1000);
        final Account.Card Maestro = accountVTB.new Card("3340 7845 8956 8906");

        final Account accountSberbank = new Account("2234234534564567", "Tatiana Iniushkina", 1000);
        final Account.Card Mir = accountSberbank.new Card("3340 7845 8956 8906");
        final Account.Card Mastercard = accountSberbank.new Card("3340 7845 8956 8906");
        operation(accountVTB,accountSberbank, Mir, Maestro, Mastercard);
    }

    public static void operation(Account accountVTB,Account accountSberbank,Account.Card Mir,Account.Card Maestro,Account.Card Mastercard) {
        Scanner sc = new Scanner(System.in);
        int selecting = 0;
        do {
            System.out.println("Сумма ,которая на балансе Сбербанка: " + accountSberbank.getAmount());
            System.out.println("Сумма ,которая на балансе VTB: " + accountVTB.getAmount());

            System.out.println("Что вы хотите сделать? \n 0.Ничего \n 1.Положить деньги на Sberbank \n 2.Вывести деньги с Sberbank \n 3.Положить деньги на VTB \n 4.Вывести деньги с VTB");
            selecting = sc.nextInt();

            if (selecting == 1) {
                System.out.println("Сколько вы хотите положить?");
                int put = sc.nextInt();
                if (errorResponse(Mir.put(put)) == 1) {
                    System.out.println("Сумма ,на которую пополнили " + put + " которая стала на балансе после того как положили: " + accountSberbank.getAmount());
                }
            }

            if (selecting == 2) {
                System.out.println("Сколько вы хотите снять?");
                int withdraw = sc.nextInt();
                if (errorResponse(Mastercard.withdraw(withdraw)) == 1) {
                    System.out.println("Сумма,которую сняли " + withdraw + " которая стала на балансе: " + accountSberbank.getAmount() + "\n");
                }
            }

            if (selecting == 3) {
                System.out.println("Сколько вы хотите положить? ");
                int putVTB = sc.nextInt();
                if (errorResponse(Maestro.put(putVTB)) == 1) {
                    System.out.println("Сумма ,на которую пополнили " + putVTB + " которая стала на балансе после того как положили: " + accountVTB.getAmount());
                }
            }

            if (selecting == 4) {
                System.out.println("Сколько вы хотите снять? ");
                int withdrawVTB = sc.nextInt();
                if (errorResponse(Maestro.withdraw(withdrawVTB)) == 1) {
                    System.out.println("Сумма,которую сняли " + withdrawVTB + "которая стала на балансе: " + accountVTB.getAmount());
                }
            }
        } while (selecting > 0 && selecting < 4);
    }

    private static int errorResponse(int methodOperation) {
        int num = 1;
        if (methodOperation == 1) {
            System.out.println("Ошибка!введите другую сумму!");
            return 0;
        }
        if (methodOperation == 2) {
            System.out.println("Ошибка!Такой суммы нет,бери что есть!");
            return 0;
        }
        return num;
    }
}
