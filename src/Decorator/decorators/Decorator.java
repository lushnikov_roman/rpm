package Decorator.decorators;

import Decorator.product.Component;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public abstract void afterTopping();

    @Override
    public void topping() {
        component.topping();
        afterTopping();
    }
}