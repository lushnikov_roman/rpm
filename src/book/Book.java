package book;

public class Book {
    private String book;
    private String author;
    private int date;
    private String url;
    private Genre genre;

    public Book(String book, String author, int date, String url, Genre genre) {
        this.book = book;
        this.author = author;
        this.date = date;
        this.url = url;
        this.genre = genre;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getBook() {
        return book;
    }

    public String getAuthor() {
        return author;
    }

    public int getDate() {
        return date;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Book{" +
                "book='" + book + '\'' +
                ", author='" + author + '\'' +
                ", date=" + date +
                ", url='" + url + '\'' +
                ", жанр='" + genre + '\''+
                '}';
    }
}
