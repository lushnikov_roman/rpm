package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MacAddressValidityExample13 {
    public static void main(String[] args) {
                int counter = 0;
                String string = "77:a3:d2:01:ff:63";
                Pattern pattern1 = Pattern.compile("^(((\\p{XDigit}{2})[:-]){5}\\p{XDigit}{2})$");
                Matcher matcher1 = pattern1.matcher(string);

                while (matcher1.find()) {
                    counter++;
                    System.out.println("Match found " + string.substring(matcher1.start(), matcher1.end()) + " Starting at index " + matcher1.start() + " and ending at index " + matcher1.end());

                }
                System.out.println("Matches found " + counter);

    }
}
